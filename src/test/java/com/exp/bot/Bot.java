package com.exp.bot;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

public class Bot {

	@Test
	public void TestBot() {
		System.setProperty("webdriver.chrome.driver", "./chromedriver");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--allow-insecure-localhost");
		options.addArguments("--no-sandbox");
		options.addArguments("--disable-dev-shm-usage");
		options.addArguments("--headless");
		try {
			System.out.println("Starting Execution...");
			WebDriver driver = new RemoteWebDriver(new URL("http://172.17.0.2:4444/"), options);
			driver.get("https://www.google.com/");
			driver.findElement(By.name("q")).sendKeys("Wassup!");
			TakesScreenshot scrShot =((TakesScreenshot)driver);
			File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
			File DestFile=new File(System.getProperty("user.dir")+"/target/screesnshot.png");
			FileUtils.copyFile(SrcFile, DestFile);
			System.out.println("EXECUTION COMPLETE!!");
			driver.close();
			driver.quit();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
